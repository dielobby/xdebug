#
# Cookbook Name:: xdebug
# Recipe:: default
#
# Copyright Paul Ilea
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

if node['platform_version'] == '16.04'
	package 'php-xdebug' do
		action 'upgrade'
	end

	template '/etc/php/' + node['ubuntu_base']['php_version'] + '/mods-available/xdebug.ini' do
		mode '0644'
	end
else
	package 'php5.6-xdebug'

	template '/etc/php5/apache2/conf.d/xdebug.ini' do
		mode '0644'
		ignore_failure true
	end

	template '/etc/php5/cli/conf.d/xdebug.ini' do
		mode '0644'
		notifies :restart, 'service[apache2]', :delayed
		ignore_failure true
	end

	template '/etc/php/5.6/apache2/conf.d/xdebug.ini' do
		mode '0644'
		ignore_failure true
	end

	template '/etc/php/5.6/cli/conf.d/xdebug.ini' do
		mode '0644'
		notifies :restart, 'service[apache2]', :delayed
		ignore_failure true
	end
end
