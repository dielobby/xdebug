default['xdebug']['directives'] = {
	:remote_enable => 1,
	:remote_connect_back => 1,
	:remote_port => 9000,
	:remote_handler => 'dbgp',
	:profiler_enable => 0,
	:profiler_enable_trigger => 1
}
