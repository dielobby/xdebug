name 'xdebug'
maintainer 'Paul Ilea'
maintainer_email 'p.ilea@die-lobby.de'
license 'MIT'
description 'Installs/configures XDebug.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.0'
issues_url 'https://bitbucket.org/dielobby/xdebug/issues'
source_url 'https://bitbucket.org/dielobby/xdebug'
recipe 'xdebug::default', 'Installs/configures XDebug.'

depends 'ubuntu_base'
